<?php
?>
<?php echo "<!-- START " . basename('box.tpl') . " -->"; ?>

<div class="box">
<?php if ($title): ?>
  <h2><?php print $title; ?></h2>
<?php endif; ?>
  <div class="content">
<?php print $content; ?>
  </div>
</div>
<?php echo "<!-- END " . basename('box.tpl') . " -->"; ?>

