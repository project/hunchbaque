<?php

/*
 * New regions for blocks are easy to add. Just put it in here so you have a 
 * new place for extra blocks. The first part is the name of the variable to 
 * place into your page.tpl.php and the second part is the human readable name 
 * that will show up in your blocks administration. You may notice that I have 
 * not used the traditional right_sidebar and left_sidebar. The reason for this 
 * is I want people to break the mindset that the regions are only for 
 * sidebars. This theme does use them for such since I wanted to provide a 
 * template that had the proper CSS for compensating for appearing and 
 * disappearing sidebars.
 */

function hunchbaque_regions() {
  return array(
    'main_supplements' => t('Primary blocks'),
    'secondary_supplements' => t('Secondary blocks'),
    'footer' => t('Footer')
  );
} 

/*
 * In working more and more with Drupal in a team environment and with version
 * control, one stylesheet can be a bit of a bottleneck. Only one person can 
 * really work on the theming. Any more than that, there are version conflicts 
 * and it just ends up being a mess. Beings Drupal now has the handy function 
 * to consolidate all CSS into onw cached file, we may as well take advantage 
 * of it and split things up so multiple people can work on a theme at once. We 
 * will assume you will be putting all stylesheets in the css folder of the 
 * theme since we want to keep things tidy. So in the array below, just add the 
 * name of the stylesheet you want to load with the page. The function below 
 * will take care of the rest.
 */

function hunchbaque_theme_styles() {
  $output['screen, projection'] = array(
    'reset.css',
    'typography.css',
    'forms.css',
    'screen-layout.css',
    'screen-block.css',
  );
  $output['print'] = array(
    'print-layout.css',
    'print.css',
  );
  $output['handheld'] = array(
    'handheld-layout.css',
  );
  return $output;
}

/*
 * Support for IE specific stylesheets. Just use the 'ie' (ie. $output['ie']) 
 * array for all versions of Internet Explorer of the version number (ie. 
 * $output['6']). Support for lt, lte, gt and gte will come at a later date. 
 */

function hunchbaque_ie_styles() {
  $output['ie'] = array(
    'ie.css',
  );
  
  return $output;
}

function hunchbaque_styles() {
  $theme_css = hunchbaque_theme_styles();
  foreach ($theme_css as $media => $value) {
    foreach ($value as $stylesheet) {
      drupal_add_css(path_to_theme().'/'.$stylesheet, 'theme', $media, 'true');
    }    
  }
  $css = drupal_add_css();
  print drupal_get_css($css);
  
  $ie_css = hunchbaque_ie_styles();
  $output = "\n";
  foreach ($ie_css as $version => $value) {
    if($version == 'ie'){
      $output .= '<!--[if IE]>'."\n";
    } else {
      $output .= '<!--[if IE '.$version.']>'."\n";
    }
    foreach ($value as $stylesheet) {
      $output .= '  <style type="text/css" media="screen, projection">@import "'.base_path().path_to_theme().'/'.$stylesheet.'";</style>'."\n";
    }
    $output .= '<![endif]-->'."\n";
  }
  print $output;
}

/*
 * The below function is to construct a set of classes for the body tag. I will 
 * admit, this is pretty much taken from the Zen theme. I have added some minor 
 * changes to make it easier to compensate for additional regions that you may 
 * want to put into your template. I have documented those further down below.
 */

function _phptemplate_variables($hook, $vars = array()) {
  switch ($hook) {
    // Send a new variable, $logged_in, to page.tpl.php to tell us if the current user is logged in or out.
    case 'page':
      // get the currently logged in user
      global $user;
      
      // An anonymous user has a user id of zero.      
      if ($user->uid > 0) {
        // The user is logged in.
        $vars['logged_in'] = TRUE;
      }
      else {
        // The user has logged out.
        $vars['logged_in'] = FALSE;
      }
      
      $body_classes = array();
      // classes for body element
      // allows advanced theming based on context (home page, node of certain type, etc.)
      $body_classes[] = ($vars['is_front']) ? 'front' : 'not-front';
      $body_classes[] = ($vars['logged_in']) ? 'logged-in' : 'not-logged-in';
      if ($vars['node']->type) {
        $body_classes[] = 'ntype-'. hunchbaque_id_safe($vars['node']->type);
      }
      
      //  This following bit of code sets flags for if sidebars exist. If you 
      //  have multiple regions in the sidebars, add them to the conditional. 
      //  The first is for the left side and the second for the right.
      if ($vars['search_box']||$vars['main_supplements']) {
        $leftBar = true;
      }
      
      if ($vars['secondary_supplements']) {
        $rightBar = true;
      }
      
      
      switch (TRUE) {
      	case $leftBar && $rightBar :
      		$body_classes[] = 'sidebars';
      		break;
      	case $leftBar :
      		$body_classes[] = 'main-sidebar';
      		break;
      	case $rightBar :
      		$body_classes[] = 'secondary-sidebar';
      		break;
      }
      // implode with spaces
      $vars['body_classes'] = implode(' ', $body_classes);
      
      break;
      
    case 'node':
      // get the currently logged in user
      global $user;

      //  set a new $is_admin variable
      //  this is determined by looking at the currently logged in user and 
      //  seeing if they are in the role 'admin'
      $vars['is_admin'] = in_array('admin', $user->roles);
      
      $node_classes = array('node');
      if ($vars['sticky']) {
      	$node_classes[] = 'sticky';
      }
      if (!$vars['node']->status) {
      	$node_classes[] = 'node-unpublished';
      }
      $node_classes[] = 'ntype-'. hunchbaque_id_safe($vars['node']->type);
      // implode with spaces
      $vars['node_classes'] = implode(' ', $node_classes);
      
      break;
      
    case 'comment':
      // we load the node object that the current comment is attached to
      $node = node_load($vars['comment']->nid);
      // if the author of this comment is equal to the author of the node, we set a variable
      // then in our theme we can theme this comment differently to stand out
      $vars['author_comment'] = $vars['comment']->uid == $node->uid ? TRUE : FALSE;
      break;
  }
  
  return $vars;
}

function hunchbaque_id_safe($string) {
  if (is_numeric($string{0})) {
    // if the first character is numeric, add 'n' in front
    $string = 'n'. $string;
  }
  return strtolower(preg_replace('/[^a-zA-Z0-9-]+/', '-', $string));
}

/*
 * The next three functions sole purpose is to make a better menu. In a 
 * nutshell, it applies "first" and "last" classes like in the primary and 
 * secondary menus.
 */

function phptemplate_menu_tree($pid = 1) {
  if ($tree = phptemplate_menu_tree_improved($pid)) {
    return "\n<ul class=\"menu\">\n". $tree ."</ul>\n";
  }
}

function phptemplate_menu_tree_improved($pid = 1) {
  $menu = menu_get_menu();
  $output = '';

  if (isset($menu['visible'][$pid]) && $menu['visible'][$pid]['children']) {
    $num_children = count($menu['visible'][$pid]['children']);
    for ($i=0; $i < $num_children; ++$i) {
      $mid = $menu['visible'][$pid]['children'][$i];
      $type = isset($menu['visible'][$mid]['type']) ? $menu['visible'][$mid]['type'] : NULL;
      $children = isset($menu['visible'][$mid]['children']) ? $menu['visible'][$mid]['children'] : NULL;
      $extraclass = $i == 0 ? 'first' : ($i == $num_children-1 ? 'last' : '');
      $output .= theme('menu_item', $mid, menu_in_active_trail($mid) || ($type & MENU_EXPANDED) ? theme('menu_tree', $mid) : '', count($children) == 0, $extraclass);     
    }
  }

  return $output;
}

function phptemplate_menu_item($mid, $children = '', $leaf = TRUE, $extraclass = '') {
  return '  <li class="'. ($leaf ? 'leaf' : ($children ? 'expanded' : 'collapsed')) . ($extraclass ? ' ' . $extraclass : '') . '">'. menu_item_link($mid, TRUE, $extraclass) . $children ."</li>\n";
}

/*
 * This is being reworked to accommodate the hCard microformat. Now all 
 * contacts will show as an exportable in supporting browsers and browser 
 * addons.
 * 
 * While I was at it, I removed the "(Not Verified)" text and instead added 
 * classes for verified and not-verified users. 
 */
function phptemplate_username($object) {

  if ($object->uid && $object->name) {
    // Shorten the name when it is too long or it will break many tables.
    if (drupal_strlen($object->name) > 20) {
      $name = drupal_substr($object->name, 0, 15) .'...';
    }
    else {
      $name = $object->name;
    }

    if (user_access('access user profiles')) {
      $output = '<span class="vcard author">'.l($name, 'user/'. $object->uid, array('class' => 'fn url verified','title' => "$name".t("'s profile."))).'</span>';
    }
    else {
      $output = '<span class="vcard author"><span class="fn">'.check_plain($name).'</span></span>';
    }
  }
  else if ($object->name) {
    // Sometimes modules display content composed by people who are
    // not registered members of the site (e.g. mailing list or news
    // aggregator modules). This clause enables modules to display
    // the true author of the content.
    if ($object->homepage) {
      $output = '<span class="vcard author">'.l($object->name, $object->homepage, array('class' => 'fn url not-verified', 'title' => $object->name.t("'s homepage"), 'rel' => 'no-follow')).'</span>';
    }
    else {
      $output = check_plain($object->name);
    }

  }
  else {
    $output = '<span class="vcard author"><span class="fn">'.variable_get('anonymous', t('Anonymous')).'</span></span>';
  }

  return $output;
}

/* 
 * This is a bit of code so that one can easily edit the code that appears 
 * around all of the comments in a theme. This is not to be confused with what 
 * comment.tpl.php does. That is for individual comments.
 */

function phptemplate_comment_wrapper($content, $type = null) {
  static $node_type;
  if (isset($type)) $node_type = $type;

  if (!$content || $node_type == 'forum') {
    return "<div id=\"comments\">\n  ". $content . "\n</div>\n";
  }
  else {
    return "<div id=\"comments\">\n  <h2 class=\"comments\">". t('Comments') ."</h2>\n  <div class=\"hfeed\">". $content ."  </div>\n</div> <!-- /#comments -->\n\n";
  }
}
