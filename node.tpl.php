<?php
?>
<?php echo "<!-- START " . basename('node.tpl') . " -->"; ?>

<div id="node-<?php print $node->nid; ?>" class="<?php print $node_classes ?><?php if ($page == 0) print ' hentry'; ?>">
<?php if ($page == 0): ?>
  <h2 class="title entry-title"><a href="<?php print $node_url ?>" rel="bookmark"><?php print $title; ?></a></h2>
<?php endif; ?>
<?php if ($picture) print $picture; ?>
<?php if ($submitted): ?>
  <span class="submitted "><?php print t('Posted') ?> <abbr class="published" title="<?php print format_date($node->created, 'custom', "Y-m-d").'T'.format_date($node->created, 'custom', "H:i:s"); ?>"><?php print format_date($node->created, 'custom', "F jS, Y"); ?></abbr> <?php print t('by'); ?> <?php print theme('username', $node); ?></span>
<?php endif; ?>
<?php if (count($taxonomy)): ?>
  <div class="taxonomy tags">
<?php print t(' in ') . $terms ?>
  </div>
<?php endif; ?>
  <div class="content entry-content">
<?php print $content; ?>
  </div>
<?php if ($links): ?>
  <div class="links">
<?php print $links; ?>

  </div>
<?php endif; ?>
</div> <!-- /#node-<?php print $node->nid; ?> -->
<?php echo "<!-- END " . basename('node.tpl') . " -->"; ?>


