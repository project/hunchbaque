<?php
?>
<?php echo "<!-- START " . basename('comment.tpl') . " -->"; ?>
<div class="comment<?php if ($comment->status == COMMENT_NOT_PUBLISHED) print ' comment-unpublished'; ?><?php if ($new != '') print ' comment-new'; ?> <?php print $zebra; ?> hentry">
  <h3 class="title entry-title"><?php print l($comment->subject, $base_path.'node/'.$comment->nid, array('rel' => 'bookmark self'), NULL, 'comment-'.$comment->cid); ?></h3>
<?php if ($new != ''): ?>
  <span class="new"><?php print $new; ?></span>
<?php endif; ?>
<?php if ($picture) print $picture; ?>
  <span class="submitted "><?php print t('Submitted on ') ?> <abbr class="published" title="<?php print format_date($comment->timestamp, 'custom', "Y-m-d").'T'.format_date($comment->timestamp, 'custom', "H:i:s"); ?>"><?php print format_date($comment->timestamp, 'custom', "F jS, Y"); ?></abbr> <?php print t('by'); ?> <?php print theme('username', $comment); ?></span>
  <div class="content entry-content">
<?php print $content; ?>
  </div>
  <div class="links">
<?php print $links; ?>

  </div>
</div>
<?php echo "<!-- END " . basename('comment.tpl') . " -->"; ?>

