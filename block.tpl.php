<?php
?>
<?php echo "<!-- START " . basename('block.tpl') . " -->"; ?>

<div id="block-<?php print $block->module .'-'. $block->delta; ?>" class="block block-<?php print $block->module ?>">
  <h2 class="title"><?php print $block->subject; ?></h2>
  <div class="content">
    <?php print $block->content; ?>

  </div>
</div> <!-- /#block-<?php print $block->module .'-'. $block->delta; ?> -->
<?php echo "<!-- END " . basename('block.tpl') . " -->"; ?>

